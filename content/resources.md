---
title: "Resources"
menu: "main"
---
# Resources

## Academic resources

#### Student Packages

####

### Coding
Codecademy is a website that offers free coding classes in 12 different programming languages including Python, Java, PHP, JavaScript, Ruby, SQL and Sass, as well as HTML and CSS.

### UX

### UI

## Social resources

### Women


#### Women in STEM
Code Like a Girl
VicICT4Women

### LGBTQIA
(i.e. Youth groups)
https://minus18.org.au/
http://globalqueermuslims.net/

#### LGBTQIA in STEM
GLEAM

### Disabilities
STEPPING INTO Internships 

### Religious/Cultural Groups


## Career resources

### LGBT
Pride in DIvesrity 
http://www.prideinclusionprograms.com.au/list-of-sport-members/

